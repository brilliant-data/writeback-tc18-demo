create table if not exists sales_by_month (
  id serial not null primary key,
  system_name text,
  port_location text,

  product_name text,

  month_start date,

  quantity numeric(10,0),
  unit_price numeric(10,2),

  comment text

);


create table if not exists sales_by_month_audit (
  id serial not null primary key,
  change_at timestamp,
  user_name text,
  target_id integer,
  new_quantity numeric(10,0),
  new_comment text
);
