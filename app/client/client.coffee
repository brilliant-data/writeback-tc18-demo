# Keep the current chart so we can manip it later
#myChart = null

TABLEAU_NULL = '%null%'


# Because handlers in promises swallow errors and
# the error callbacks for Promises/A are flaky,
# we simply use this function to wrap calls
errorWrapped = (context, fn)->
  (args...)->
    try
      fn(args...)
    catch err
      console.error "Got error during '", context, "' : ", err


# Takes a table and returns a "COLUMN_NAME" => COLUMN_IDX map
getColumnIndexes = (table, required_keys)->
  # Create a column name -> idx map
  colIdxMaps = {}
  for c in table.getColumns()
    fn = c.getFieldName()
    if fn in required_keys
      colIdxMaps[fn] = c.getIndex()
  colIdxMaps

# Takes a Tableau Row and a "COL_NAME" => COL_IDX map and returns
# a new object with the COL_NAME fields set to the corresponding values
convertRowToObject = (row, attrs_map)->
  o = {}
  for name, id of attrs_map
    o[name] = row[id].value
  o


# Updates form fields in parent selector from a hash of name => value
# pairs
updateFormFields = (parent, data)->
  $parent = $(parent)
  for k,v of data
    # Skip the tableau %null%-S
    v = "" if v == TABLEAU_NULL
    $("input[name=#{k}], textarea[name=#{k}]", $parent.el).val(v)
    $("[data-field=#{k}]", $parent.el).text(v)

getFormFields = (parent)->
  o = {}
  $("input, textarea, [data-field]").each ()->
    $t = $(this)
    o[$t.attr('name')] = $t.val()
  o


sheetToRefresh = null
# FORM TOOLS
# ==========

submitForm = (e)->
  # dont follow up
  e.preventDefault()
  # Collect the form data
  formData = getFormFields('#editor-form')
  # replace the submit url with the proper fields
  submit_url = $(this).data('url').replace /\{\{([a-z_]+)\}\}/g, (m, name)-> formData[name]

  $.get(submit_url, _.pick(formData, "id", "quantity", "comment", "user"))
    .done ()->
      # Update the tableau workbook after we have the data
      if sheetToRefresh
        sheetToRefresh.getDataSourcesAsync()
          .then (dataSources)-> Promise.all(dataSources.forEach( (ds)-> ds.refreshAsync()))
          .catch (err) -> console.log("ERROR: " + err)

    .fail (err)-> console.error "Error getting the data:", err.message, err.stack


initEditorForm = (selector)->
  $editorForm = $(selector)
  $("[data-submit=true]", $editorForm.el).click submitForm
  $editorForm


showEditor = ()-> window.parent.$('#tabZoneId3').show(500)
hideEditor = ()-> window.parent.$('#tabZoneId3').hide(500)
toggleEditor = (show)-> if show then showEditor() else hideEditor()

unregisterHandlerFunctions = []
registerMarkChangeHandlers = (handler)->
  # Unregister all
  unregisterHandlerFunctions.forEach((fn)-> fn())

  dashboard = tableau.extensions.dashboardContent.dashboard
  dashboard.worksheets.forEach (worksheet)->
    console.log("Sheet")

    #
    unregisterFn = worksheet.addEventListener tableau.TableauEventType.MarkSelectionChanged, (e)->
      console.log("mark change")
      handler(e)
    # add to unregister handlers
    unregisterHandlerFunctions.push unregisterFn


# TABLEAU HOOKS
# ============

columnNamesMap =
  SystemName: 'system_name'
  PortLocation: 'port_location'
  UnitPrice: 'unit_price'
  Quantity: 'quantity'
  Comment: 'quantity'

initEditor = ->
  $editorForm = initEditorForm("#editor-form")


  # Error handler in case getting the data fails in the Promise
  onDataLoadError = (err)->
    console.log("Error during Tableau Async request:" + err)

  # Handler for loading and converting the tableau data to chart data
  onDataLoadOk = (table)->
    console.log("DATA OK")
    console.log()



    data = table.data.map (row)->
      o = {}
      row.forEach (v,i)->
        console.log("Idx=" + i)
        console.log("Value = " + v)
        column = table.columns[i]
        console.log("Column = " + JSON.stringify(column))
        remappedName = column.fieldName.toLowerCase().replace(/[^a-z0-9]+/g, '_')
        o[remappedName] = v.value
      o

    console.log("Data = " + JSON.stringify(data))
    toggleEditor(data.length == 1)

    if parent && parent.tsConfig
      data.user = parent.tsConfig.current_user_name

    if data.length == 1
      updateFormFields $editorForm, data[0]


    # errorWrapped "Getting data from Tableau", (table)->
    #   # Decompose the ids
    #   col_indexes = getColumnIndexes(table, ["id", "month_start", "system_name", "port_location", "product_name", "quantity", "unit_price", "comment"])

    #   data = table.getData()

    #   # Show-hide the editor if we have data
    #   toggleEditor(data.length == 1)

    #   graphDataByCategory = _.chain(table.getData())
    #     .map (row)-> convertRowToObject(row, col_indexes)
    #     .first()
    #     .value()

    #   errorWrapped( "Updating form fields", updateFormFields)( $editorForm, graphDataByCategory )

  # Handler that gets the selected data from tableau and sends it to the chart
  # display function
  updateEditor = (sheet)->
    sheetToRefresh = sheet
    sheet
      .getUnderlyingDataAsync({maxRows: 1, ignoreSelection: false, includeAllColumns: true, ignoreAliases: true})
      .then(onDataLoadOk)
      .catch(onDataLoadError)

  registerMarkChangeHandlers (e)->
    try
      updateEditor(e.sheet)
    catch e
      console.log("ERROR: " + e)


  # # Add an event listener for marks change events that simply loads the
  # # selected data to the chart
  # getCurrentViz().addEventListener( tableau.TableauEventName.MARKS_SELECTION,  updateEditor)


$ ->
  tableau.extensions.initializeAsync().then ->
    window.parent.$('#tabZoneId3').hide(0)
    initEditor()

@appApi = {
  initEditor
}
